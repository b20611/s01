import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

        Scanner userDetails = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = userDetails.nextLine();

        System.out.println("Last Name:");
        String lastName = userDetails.nextLine();

        System.out.println("First Subject Grade:");
        double grade1 = userDetails.nextDouble();

        System.out.println("Second Subject Grade:");
        double grade2 = userDetails.nextDouble();

        System.out.println("Third Subject Grade:");
        double grade3 = userDetails.nextDouble();

        System.out.println("Good day, " + firstName + " " + lastName + ".");

        double average = (grade1 + grade2 + grade3)/3;

        System.out.println("Your grade average is:" + average);
    }
}